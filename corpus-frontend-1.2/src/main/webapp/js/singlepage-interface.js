/* global BLS_URL, URI, PROPS_IN_COLUMNS saveAs */

/**
 * Dictionary where key == propertyId and value is array of containing the values for that id, in order of occurance.
 * Available properties are contained in SINGLEPAGE.INDEX.complexFields['complexFieldId'].properties['propertyId']
 * NOTE: starting with blacklab 2.0 (and dev versions after 29-08-2018) properties are now called annotations and
 * are contained in INDEX.annotatedFields['id'].annotations['id']
 * 
 * The 'punct' property is always available.
 *
 * @typedef {Object.<string, Array.<string>} BLHitContext
 */

/**
 * @typedef {Object} BLHit
 *
 * @property {string} docPid - id of the document from which the hit originated
 * @property {number} start - index of first token in hit, inclusive
 * @property {number} end - index of last token in hit, exclusive
 * @property {BLHitContext} left - context before the hit
 * @property {BLHitContext} match - context of the hit, may contain more than 1 value per property if query matched multiple tokens
 * @property {BLHitContext} right - context after the hit
 */

var SINGLEPAGE = SINGLEPAGE || {};

/**
 * Responsible for converting search results to html.
 * Also contains functions to clear old results from the page.
 * Does not manage the main search form.
 */
SINGLEPAGE.INTERFACE = (function() {
	'use strict';

	var ELLIPSIS = String.fromCharCode(8230);


	/**
	 * @typedef {Object} Property
	 * @property {string} id
	 * @property {string} displayName
	 * @property {boolean} isMainProp
	 */

	/** @type {{all: Property[], shown: Property[], firstMainProp: Property  }} */
	var PROPS = {};
	// Gather up all relevant properties of words in this index
	PROPS.all = $.map(SINGLEPAGE.INDEX.complexFields || SINGLEPAGE.INDEX.annotatedFields, function(complexField) {
		return $.map(complexField.properties || complexField.annotations, function(prop, propId) {
			if (prop.isInternal)
				return null; // skip prop
			return {
				id: propId,
				displayName: prop.displayName || propId,
				isMainProp: propId === complexField.mainProperty
			};
		});
	});

	/** Columns configured in PROPS_IN_COLUMNS, can contain duplicates */
	PROPS.shown = $.map(PROPS_IN_COLUMNS, function(propId) {
		return $.map(PROPS.all, function(prop) {
			return (prop.id === propId) ? prop : undefined;
		});
	});

	// There is always at least a single main property.
	// TODO this shouldn't be required, mainProperties from multiple complexFields should be handled properly
	// but this has some challenges in the hits table view, such as that it would show multiple columns for the before/hit/after contexts
	PROPS.firstMainProp = PROPS.all.filter(function(prop) { return (prop.id = 'word'); })[0];

	// Add a 'hide' function to bootstrap tabs
	// Doesn't do more than remove classes and aria labels, and fire some events
	$.fn.tab.Constructor.prototype.hide = function() {
		var $this    = this.element;
		var selector = $this.data('target') || $this.attr('href');

		var hideEvent = $.Event('hide.bs.tab', {
			relatedTarget: $this[0]
		});

		$this.trigger(hideEvent);
		if (hideEvent.isDefaultPrevented())
			return;

		$this.closest('li.active')
			.removeClass('active')
			.attr('aria-expanded', false)
			.trigger({
				type: 'hidden.bs.tab',
				relatedTarget: this[0]
			});
		$(selector).removeClass('active');
	};

	/**
	 * @param {BLHitContext} context
	 * @param {string} prop - property to retrieve
	 * @param {boolean} doPunctBefore - add the leading punctuation?
	 * @param {string} addPunctAfter - trailing punctuation to append
	 * @returns {string} concatenated values of the property, interleaved with punctuation from context['punt']
	 */
	function words(context, prop, doPunctBefore, addPunctAfter) {
		var parts = [];
		var n = context[prop] ? context[prop].length : 0;
		for (var i = 0; i < n; i++) {
			if ((i == 0 && doPunctBefore) || i > 0)
				parts.push(context.punct[i]);
			parts.push(context[prop][i]);
		}
		parts.push(addPunctAfter);
		return parts.join('');
	}

	/**
	 * @param {BLHit} hit - the hit
	 * @param {string} [prop] - property of the context to retrieve, defaults to PROPS.firstMainProp (usually 'word')
	 * @returns {Array.<string>} - string[3] where [0] == before, [1] == hit and [2] == after, values are strings created by
	 * concatenating and alternating the punctuation and values itself
	 */
	function snippetParts(hit, prop) {
		prop = prop || PROPS.firstMainProp.id;

		var punctAfterLeft = hit.match.word.length > 0 ? hit.match.punct[0] : '';
		var before = words(hit.left, prop, false, punctAfterLeft);
		var match = words(hit.match, prop, false, '');
		var after = words(hit.right, prop, true, '');
		return [before, match, after];
	}

	/**
	 * Concat all properties in the context into a large string
	 *
	 * @param {BLHitContext} context
	 * @returns {string}
	 */
	function properties(context) {
		var props = [];
		for (var key in context) {
			if (context.hasOwnProperty(key)) {
				var val = $.trim(context[key]);
				if (!val) continue;
				props.push(key+': '+val);
			}
		}
		return props.join(', ');
	}

	/**
	 * Fade out the table, then replace its contents, and call a function.
	 *
	 * @param {object} $table
	 * @param {string} html Table head and body
	 * @param {function} [onComplete] callback, will be called in the context of $table
	 */
	function replaceTableContent($table, html, onComplete) {
		// skip the fadeout if the table is empty
		// fixes annoying mini-delay when first viewing results
		var fadeOutTime = $table.find('tr').length ? 200 : 0;

		$table.animate({opacity: 0}, fadeOutTime, function () {
			$table.html(html);
			if (onComplete)
				onComplete.call($table);
			$table.animate({opacity: 1}, 200);
		});
	}

	/**
	 * Request and display more preview text from a document.
	 *
	 * @param {any} concRow the <tr> element for the current hit. The result will be displayed in the row following this row.
	 * @param {any} docPid id/pid of the document
	 * @param {number} start
	 * @param {number} end
	 * @param {('ltr' | 'rtl')} textDirection - to determine whether to specify text direction on the preview text
	 */
	function showCitation(concRow, docPid, start, end, textDirection) {
		// Open/close the collapsible in the next row
		var $element = $(concRow).next().find('.collapse');
		$element.collapse('toggle');

		$.ajax({
			url: BLS_URL + 'docs/' + docPid + '/snippet',
			dataType: 'json',
			data: {
				hitstart: start,
				hitend: end,
				wordsaroundhit: 50
			},
			success: function (response) {
				var parts = snippetParts(response);
				$element.html('<span dir="'+ textDirection+'"><b>Kwic: </b>'+ parts[0] + '<b>' + parts[1] + '</b>' + parts[2]+ '</span>');
			},
			error: function(jqXHR, textStatus/*, errorThrown*/) {
				$element.text('Error retrieving data: ' + (jqXHR.responseJSON && jqXHR.responseJSON.error) || textStatus);
			}
		});
	}

	/**
	 * Request and display properties of the matched word.
	 *
	 * @param {any} propRow the <tr> element for the current hit. The result will be displayed in the second row following this row.
	 * @param {any} props the properties to show
	 */
	function showProperties(propRow, props) {
		// Open/close the collapsible in the next row
		var $element = $(propRow).next().next().find('.collapse');
		$element.collapse('toggle');

		var $p = $('<div/>').text(props).html();
		$element.html('<span><b>Properties: </b>' + $p + '</span>');
	}

	/**
	 * Show the error reporting field and display any errors that occured when performing a search.
	 *
	 * Can be directly used as callback fuction to $.ajax
	 *
	 * @param {any} jqXHR
	 * @param {any} textStatus
	 * @param {any} errorThrown
	 */
	function showBlsError(jqXHR, textStatus, errorThrown) {
		var errordata = (jqXHR && jqXHR.responseJSON && jqXHR.responseJSON.error) || {
			'code': 'WEBSERVICE_ERROR',
			'message': 'Error contacting webservice: ' + textStatus + '; ' + errorThrown
		};

		$('#errorDiv').text(errordata.message + ' (' + errordata.code + ') ').show();
	}

	/**
	 * Hide the error field.
	 */
	function hideBlsError() {
		$('#errorDiv').text('(error here)').hide();
	}

	/**
	 * Create pagination buttons based on a set of results.
	 *
	 * Buttons contain a data-page attribute containing the page index they're displaying - 1.
	 *
	 * @param {any} $pagination <ul> element where the generated pagination will be placed.
	 * @param {any} data a blacklab-server search response containing either groups, docs, or hits.
	 */
	function updatePagination($pagination, data) {
        
		var beginIndex = data.summary.windowFirstResult;
		var pageSize = data.summary.requestedWindowSize;
		var totalResults;
		if (data.summary.numberOfGroups != null)
			totalResults = data.summary.numberOfGroups;
		else
			totalResults = (data.hits ? data.summary.numberOfHitsRetrieved : data.summary.numberOfDocsRetrieved);

		// when out of bounds results, draw at least the last few pages so the user can go back
		if (totalResults < beginIndex)
			beginIndex = totalResults+1;

		var totalPages =  Math.ceil(totalResults / pageSize);
		var currentPage = Math.ceil(beginIndex / pageSize);
		var startPage = Math.max(currentPage - 10, 0);
		var endPage = Math.min(currentPage + 10, totalPages);

		var html = [];
		if (currentPage == 0)
			html.push('<li class="disabled"><a>Prev</a></li>');
		else
			html.push('<li><a href="javascript:void(0)" data-page="', currentPage-1, '">Prev</a></li>');

		if (startPage > 0) {
            
            html.push('<li><a href="javascript:void(0)" data-page="', 0, '">', 1, '</a></li>');
                
			html.push('<li class="disabled"><a>...</a></li>');
        }

		for (var i = startPage; i < endPage; i++) {
			var showPageNumber = i + 1;
			if (i == currentPage)
				html.push('<li class="active"><a>', showPageNumber, '</a></li>');
			else
				html.push('<li><a href="javascript:void(0)" data-page="', i, '">', showPageNumber, '</a></li>');
		}

		if (endPage < totalPages) {
			
            html.push('<li class="disabled"><a>...</a></li>');
            
            html.push('<li><a href="javascript:void(0)" data-page="', (totalPages - 1), '">', totalPages, '</a></li>');
        }

		if (currentPage == (totalPages - 1) || totalPages == 0)
			html.push('<li class="disabled"><a>Next</a></li>');
		else
			html.push('<li><a href="javascript:void(0)" data-page="', currentPage + 1, '">Next</a></li>');

		$pagination.html(html.join(''));
	}

	/**
	 * After a small delay, clear the tab's current data and show a spinner.
	 *
	 * The delay exists because it's jarring when the user switches page and all content is removed
	 * and then displayed again within a fraction of a second.
	 *
	 * @param {any} $tab the tab content container.
	 */
	function showSearchIndicator($tab) {
		if ($tab.data('searchIndicatorTimeout') == null) {
			$tab.data('searchIndicatorTimeout', setTimeout(function() {
				$tab.find('.searchIndicator').show();
				clearTabResults($tab);
			}, 500));
		}
	}

	/**
	 * Hide any currently displayed spinner within this tab, and remove any queued spinner (see showSearchIndicator).
	 *
	 * @param {any} $tab the tab's main content container.
	 */
	function hideSearchIndicator($tab) {
		if ($tab.data('searchIndicatorTimeout') != null) {
			clearTimeout($tab.data('searchIndicatorTimeout'));
			$tab.removeData('searchIndicatorTimeout');
		}
		$tab.find('.searchIndicator').hide();
	}

	/**
	 * Load and display results within a specific group of documents/hits.
	 *
	 * This function should only be called when the containing tab currently has a groupBy clause,
	 * otherwise an invalid search will be generated.
	 *
	 * This function assumes it's being called in the context of a button/element containing a data-group-id attribute
	 * specifiying a valid group id.
	 */
	function viewConcordances() {
		var $button = $(this);
		var groupId = $button.data('groupId');

		// this parameter is local to this tab, as the other tabs are probably displaying other groups.
		$(this).trigger('localParameterChange', {
			viewGroup: groupId,
			page: 0,
			sort: null,
		});


		// initialize the display indicating we're looking at a detailed group

		// Only set the text, don't show it yet
		// (it should always be hidden, as this function is only available/called when you're not currently viewing contents of a group)
		// This is a brittle solution, but better than nothing for now.
		// TODO when blacklab-server echoes this data in the search request, set these values in setTabResults() and remove this here
		var groupName = $button.data('groupName');
		var $tab = $button.closest('.tab-pane');
		var $resultgroupdetails = $tab.find('.resultgroupdetails');
		var $resultgroupname = $resultgroupdetails.find('.resultgroupname');
		$resultgroupname.text(groupName || '');
	}

	/**
	 * Loads and displays a small amount of details about individual hits/documents within a specific group.
	 * Some data is attached to the button to track how many concordances are already loaded/are available.
	 *
	 * This function does not refresh the entire tab's contents, just inserts some extra data within a group's <tr>
	 * For the version that loads the full result set and displays the extensive information, see {@link viewConcordances}
	 *
	 * This function assumes it's being called within the context of a button element containing a data-group-id attribute.
	 * Some assumptions are also made about the exact structure of the document regarding placement of the results.
	 */
	function loadConcordances() {
		var $button = $(this);
		var $tab = $button.parents('.tab-pane').first();
		var textDirection = SINGLEPAGE.INDEX.textDirection || 'ltr';
		var groupId = $button.data('groupId');
		var currentConcordanceCount = $button.data('currentConcordanceCount') || 0;
		var availableConcordanceCount = $button.data('availableConcordanceCount') || Number.MAX_VALUE;

		if (currentConcordanceCount >= availableConcordanceCount)
			return;

		var searchParams = $.extend(
			{},
			$tab.data('defaultParameters'),
			$tab.data('parameters'),
			{
				pageSize: 20,
				page: currentConcordanceCount / 20,
				viewGroup: groupId,
				sampleSize: null,
				sampleMode: null,
				sampleSeed: null,
				sort: null
			},
			$tab.data('constParameters')
		);

		SINGLEPAGE.BLS.search(searchParams, function(data) {
			var totalConcordances = data.hits ? data.summary.numberOfHitsRetrieved : data.summary.numberOfDocsRetrieved;
			var loadedConcordances = data.summary.actualWindowSize;

			// store new number of loaded elements
			$button.data('currentConcordanceCount', currentConcordanceCount + loadedConcordances)
				.data('availableConcordanceCount', totalConcordances)
				.toggle(currentConcordanceCount + loadedConcordances < totalConcordances);

			// And generate html to display
			var html = [];
			// Only one of these will run depending on what is present in the data
			// And what is present in the data depends on the current view, so all works out
			$.each(data.hits, function(index, hit) {
				var parts = snippetParts(hit);
				var left = textDirection=='ltr'? parts[0] : parts[2];
				var right = textDirection=='ltr'? parts[2] : parts[0];
				html.push(
					'<div class="clearfix">',
						'<div class="col-xs-5 text-right">', ELLIPSIS, ' ', left, '</div>',
						'<div class="col-xs-2 text-center"><b>', parts[1], '&nbsp;', '</b></div>',
						'<div class="col-xs-5">', right, ' ', ELLIPSIS, '</div>',
					'</div>');
			});

			$.each(data.docs, function(index, doc) {
                
				var title = doc.docInfo[data.summary.docFields.titleField];
				var hits = doc.numberOfHits;
				html.push(
					'<div class="clearfix">',
						'<div class="col-xs-10"><b>', title, '&nbsp;', '</b></div>',
						'<div class="col-xs-2">', hits, '&nbsp;', '</div>',
					'</div>');
			});

			// TODO tidy up
			$button.parent().parent().append(html.join(''));
		},
		function(jqXHR, textStatus, errorThrown) {
			var errordata = (jqXHR && jqXHR.responseJSON && jqXHR.responseJSON.error) || {
				'code': 'WEBSERVICE_ERROR',
				'message': 'Error contacting webservice: ' + textStatus + '; ' + errorThrown
			};

			var html = [];
			html.push('<div>',
				'<b>Could not retrieve concordances.</b><br>');

			if (jqXHR && jqXHR.status !== 0) // server is up
				html.push('This is usually due to a misconfigured server, see ',
					'<a href="https://github.com/INL/BlackLab/blob/be5b5be75c064e87cbfc2271fd19d073f80839af/core/src/site/markdown/blacklab-server-overview.md#installation" target="_blank">here</a> for more information.');

			html.push('<hr><b>', errordata.code, '</b><br>', errordata.message, '</div>');

			// TODO tidy up
			$button.parent().parent().html(html.join(''));
		});
	}

	/**
	 * Convert a blacklab-server reply containing information about hits into a table containing the results.
	 *
	 * @param {any} data the blacklab-server response.
	 * @returns An array of html strings containing the <thead> and <tbody>, but without the enclosing <table> element.
	 */
	function formatHits(data, textDirection) {
		// TODO use mustache.js

		var html = [];
		html.push(
			'<thead><tr>',
				'<th class="text-right">',
					'<span class="dropdown">',
						'<a class="dropdown-toggle" data-toggle="dropdown">',
						textDirection=='ltr'? 'Before hit ' : 'After hit ',
						'<span class="caret"></span></a>',
						'<ul class="dropdown-menu" role="menu" aria-labelledby="left">');
						PROPS.all.forEach(function(prop) { html.push(
							'<li><a data-bls-sort="left:' + prop.id + '">' + prop.displayName + '</a></li>');
						});

						html.push(
						'</ul>',
					'</span>',
				'</th>',

				'<th class="text-center">',
					'<a data-bls-sort="hit:' + PROPS.firstMainProp.id + '"><strong>' + PROPS.firstMainProp.displayName + '<strong></a>',
				'</th>',

				'<th class="text-left">',
					'<span class="dropdown">', // span instead of div or the menu won't align with the toggle text, as the toggle container is wider than the toggle's text
						'<a class="dropdown-toggle" data-toggle="dropdown">',
						textDirection=='ltr'? 'After hit ' : 'Before hit ',
						'<span class="caret"></span></a>',
						'<ul class="dropdown-menu" role="menu" aria-labelledby="right">');

						PROPS.all.forEach(function(prop) { html.push(
							'<li><a data-bls-sort="right:' + prop.id + '">' + prop.displayName + '</a></li>');
						});

						html.push(
						'</ul>',
					'</span>',
				'</th>');

			// Not all properties have their own table columns
			PROPS.shown.forEach(function(prop) { 
                if (prop.displayName != "gapw") {
                    html.push('<th class="properties_column"><a data-bls-sort="hit:' + prop.id + '">' + prop.displayName + '</a></th>');
                }
			});

            html.push('<th class="metadata_column" style="width:100px;"><a data-bls-sort="field:search_year">Year</a></th>');
            html.push('<th class="metadata_column" style="width:175px;"><a data-bls-sort="field:author">Author</a></th>');
            html.push('<th class="metadata_column" style="width:225px;"><a data-bls-sort="field:display_title">Title</a></th>');

		html.push('</tr></thead>');

		html.push('<tbody>');
		var prevHitDocPid = null;
		var numColumns = 3 + PROPS.shown.length; // before context - hit context - after context - remaining properties

        var saveDocTitle = '';
        var saveDocAuthor = '';
        var saveDocDate = '';
        var saveDocUrl = '';
        var saveDocTcpId = '';
        
		$.each(data.hits, function(index, hit) {
			// Render a row for this hit's document, if this hit occurred in a different document than the previous

			var docPid = hit.docPid;
			if (docPid !== prevHitDocPid) {

				prevHitDocPid = docPid;

				var doc = data.docInfos[docPid];

				var docTitle = '';
                if ('display_title' in doc) {
                    docTitle = doc['display_title'][0];
                }

				var docAuthor = '';
                if ('author' in doc) {
                    docAuthor = doc['author'][0];
                }

				var docDate = '';
                if ('display_year' in doc) {
                    docDate = doc['display_year'][0];
                }
                
                var file_name_parts = doc['fromInputFile'][0].split('/');

                var tcp_id = file_name_parts[file_name_parts.length - 1].split('.')[0];

                if (tcp_id != saveDocTcpId) {
                    saveDocTcpId = tcp_id;
                }

                if (docTitle != saveDocTitle) {
                    saveDocTitle = docTitle;
                }

                if (docAuthor != saveDocAuthor) {
                    saveDocAuthor = docAuthor;
                }

                if (docDate != saveDocDate) {
                    saveDocDate = docDate;
                }

				// TODO the clientside url generation story... https://github.com/INL/corpus-frontend/issues/95
				// Ideally use absolute urls everywhere, if the application needs to be proxied, let the proxy server handle it.
				// Have a configurable url in the backend that's made available on the client that we can use here.
				var docUrl;
				switch (new URI().filename()) {
					case '':
						docUrl = new URI('../../docs/'); 
						break;
					case 'docs':
					case 'hits':
						docUrl = new URI('../docs/');
						break;
					case 'search':
					default: // some weird proxy?
						docUrl = new URI('./docs/');
						break;
				}

				docUrl = docUrl
				.absoluteTo(new URI().toString())
				.filename(docPid)
				.search({
					// parameter 'query' controls the hits that are highlighted in the document when it's opened
					'query': data.summary.searchParam.patt
				})
				.toString();

                saveDocUrl = docUrl;
			}
            
            if (window.location.toString().indexOf('/eebotcp/') > -1) {
                saveDocUrl = 'https://eplab.artsci.wustl.edu/catalog/doc/' + saveDocTcpId + '.xml';
            }

			// And display the hit itself
			var parts = snippetParts(hit);
			var left = textDirection=='ltr'? parts[0] : parts[2];
			var right = textDirection=='ltr'? parts[2] : parts[0];
			var propsWord = hit.match.punct.length > 1 ? 'not shown for multiple search terms' : properties(hit.match).replace("'","\\'").replace("&apos;","\\'").replace('"', '&quot;');

			html.push(
				'<tr class="concordance" onclick="SINGLEPAGE.INTERFACE.showCitation(this, \''
				+ docPid + '\', '+ hit.start + ', '+ hit.end + ', \'' + textDirection + '\');SINGLEPAGE.INTERFACE.showProperties(this, \''+propsWord+'\');">',
					'<td class="text-right">', ELLIPSIS, ' <span dir="', textDirection, '">', left, '</span></td>',
					'<td class="text-center"><span dir="', textDirection, '"><strong>', parts[1], '</strong></span></td>',
					'<td><span dir="', textDirection, '">', right, '</span> ', ELLIPSIS, '</td>');

            PROPS.shown.forEach(function(prop) { 
                if (prop.displayName != "gapw") {
                    html.push('<td class="toggle_match_props properties_column">', words(hit.match, prop.displayName, false, ''), '</td>');
                }
            });

            html.push('<td class="toggle_doc_info metadata_column">', saveDocDate, '</td>');
            html.push('<td class="toggle_doc_info metadata_column">', saveDocAuthor.slice(0, 25), '</td>');
            
            if (window.location.toString().indexOf('/am_ix/') > -1 ||
                window.location.toString().indexOf('/eebotcp/') > -1) {
                //if (phase_1_keys.has(saveDocTcpId) == true) {
                    
                    var id_parts = hit.match.id[0].split('-');
                        
                    var page = id_parts[0] + '-' + id_parts[1];
                    var from_token_id = saveDocTcpId + '-' + hit.match.id[0];
                    var to_token_id = saveDocTcpId + '-' + hit.match.id[hit.match.id.length - 1];

                    html.push('<td class="toggle_doc_info metadata_column">' + 
                                    '<a target="_blank" href="https://texts.earlyprint.org/works/' + 
                                    saveDocTcpId + 
                                    '.xml?page=' + page + 
                                    '&from_token_id=' + from_token_id + 
                                    '&to_token_id=' + to_token_id + '">' +
                                    saveDocTitle.slice(0, 30) + 
                                    '</a></td>');
                                    
                //}
                //else {
                //    html.push('<td class="toggle_doc_info metadata_column">' + 
                //                    '<a target="_blank" href="' + saveDocUrl + '">' +
                //                    saveDocTitle.slice(0, 30) + 
                //                    '</a></td>');
                //}
            }
            
            if (window.location.toString().indexOf('/all/') > -1) {
                html.push('<td class="toggle_doc_info metadata_column">' + 
                                '<a target="_blank" href="' + saveDocUrl + '">' +
                                saveDocTitle.slice(0, 30) + 
                                '</a></td>');
            }

            html.push('<td class="toggle_doc_info metadata_column"></td>');
            html.push('<td class="toggle_doc_info metadata_column"></td>');
            html.push('<td class="toggle_doc_info metadata_column"></td>');

            html.push('</tr>');

			// Snippet row (initially hidden)
			html.push(
				'<tr>',
					'<td colspan="', numColumns, '" class="inline-concordance"><div class="collapse">Loading...</div></td>',
				'</tr>');
			// Properties row (initially hidden)
			html.push(
				'<tr>',
					'<td colspan="', numColumns, '" class="inline-concordance"><div class="collapse">Loading...</div></td>',
				'</tr>');
		});

		html.push('</tbody>');
		return html;
	}

	/**
	 * Convert a blacklab-server reply containing information about documents into a table containing the results.
	 *
	 * @param {any} data the blacklab-server response.
	 * @returns An array of html strings containing the <thead> and <tbody>, but without the enclosing <table> element.
	 */
	function formatDocs(data, textDirection) {
        
        //$('.resultcontainer table').css({'table-layout': 'fixed',});
        
		var html = [];

		html.push(
			'<thead><tr>',
				'<th style="width:30%"><a data-bls-sort="field:author">Author</a></th>',
				'<th style="width:50%"><a data-bls-sort="field:display_title">Title</a></th>',
				'<th style="width:10%"><a data-bls-sort="field:search_year">Year</a></th>',
				'<th style="width:10%"><a data-bls-sort="numhits">Hits</a></th>',
			'</tr></thead>'
		);

		html.push('<tbody>');
        
		$.each(data.docs, function(index, doc) {

			var docPid = doc.docPid;

            var docTitle = doc.docInfo['display_title'] || 'UNKNOWN';
            var docAuthor = doc.docInfo['author'];

            //var docDate = doc.docInfo['display_year'];

            var docDate = '';
            if ('display_year' in doc.docInfo) {
                docDate = doc.docInfo['display_year'];
            }

			var docHits = doc.numberOfHits || '';

			var snippetStrings = [];
			$.each(doc.snippets, function(index, snippet) {
				var parts = snippetParts(snippet);
				snippetStrings.push(ELLIPSIS, ' ', parts[0], '<strong>', parts[1], '</strong>', parts[2], ELLIPSIS);
				return false; // only need the first snippet for now
			});

			// TODO the clientside url generation story... https://github.com/INL/corpus-frontend/issues/95
			// Ideally use absolute urls everywhere, if the application needs to be proxied, let the proxy server handle it.
			// Have a configurable url in the backend that's made available on the client that we can use here.
			var docUrl;
			switch (new URI().filename()) {
				case '':
					docUrl = new URI('../../docs/'); 
					break;
				case 'docs':
				case 'hits':
					docUrl = new URI('../docs/');
					break;
				case 'search':
				default: // some weird proxy?
					docUrl = new URI('./docs/');
					break;
			}

			docUrl = docUrl
			.absoluteTo(new URI().toString())
			.filename(docPid)
			.search({
				'query': data.summary.searchParam.patt
			})
			.toString();

			html.push(
				'<tr class="per_doc_tr concordance" onclick="javascript:handle_inline_kwic(\'' + 
                                docPid +'\', \'' + docHits + '\', \'' + docUrl + '\');">',
					'<td class="per_doc_td">', 
                            neatly_trim_string(docAuthor, 45), 
                    '</td>',
					'<td class="per_doc_td">', 
                        '<a target="_blank" href="', docUrl, '">', 
                            neatly_trim_string(docTitle, 80), 
                        '</a>',
                    '</td>',
					'<td class="per_doc_td">', docDate.slice(0, 10), '</td>',
					'<td class="per_doc_td">', 
                        docHits,
                    '</td>',
				'</tr>',
                '<tr id="doc_inline_tr_' + docPid + '" class="doc_inline_tr">',
                    '<td id="doc_inline_td_' + docPid + '"  id="" class="doc_inline_td" colspan="4">',
                    '</td>',
                '</tr>');
		});
		html.push('</tbody>');

		return html;
	}

	/**
	 * Convert a blacklab-server reply containing information about hit or document groups into a table containing the results.
	 * Some minor styling is applied based on whether the results are hits or documents.
	 *
	 * @param {any} data the blacklab-server response.
	 * @returns An array of html strings containing the <thead> and <tbody>, but without the enclosing <table> element.
	 */
	function formatGroups(data) {
		var html = [];

		html.push(
			'<thead><tr>',
				'<th style="width:30%;"><a data-bls-sort="identity">Group</a></th>',
				'<th style="width:70%;"><a data-bls-sort="numhits">Hits</a></th>',
			'</tr></thead>'
		);

		// give the display a different color based on whether we're showing hits or docs
		var displayClass = data.hitGroups ? 'progress-bar-success' : 'progress-bar-warning';
		var idPrefix = data.hitGroups ? 'hg' : 'dg'; // hitgroup : docgroup

		html.push('<tbody>');
		var groups = data.hitGroups || data.docGroups;
		$.each(groups, function(index, group) {
			var groupId = group.identity;
			var htmlId = idPrefix + index;

			var displayName = group.identityDisplay.slice(0, 45);
			var displayWidth = (group.size / data.summary.largestGroupSize) * 100;

			html.push(
				'<tr>',
					'<td>', displayName, '</td>',
					'<td>',
						'<div class="progress group-size-indicator" data-toggle="collapse" data-target="#', htmlId, '" style="cursor:pointer;">',
							'<div class="progress-bar ', displayClass, '" style="min-width: ', displayWidth, '%;">', group.size, '</div>',
						'</div>',
						'<div class="collapse inline-concordance" id="', htmlId, '">',
							'<div>',
								'<button type="button" class="btn btn-sm btn-link viewconcordances" data-group-name="', displayName, '" data-group-id="', groupId, '">&#171; View detailed concordances in this group</button> - ',
								'<button type="button" class="btn btn-sm btn-link loadconcordances" data-group-id="', groupId, '">Load more concordances...</button>',
							'</div>',
						'</div>',
					'</td>',
				'</tr>');
		});
		html.push('</tbody>');

		return html;
	}

	/**
	 * Request the currently shown results as a CSV file, and save it.
	 *
	 * 'this' should be the tab containing the results to export.
	 * 'event.target' should be the element that was clicked.
	 *
	 * @param {Jquery.Event} event
	 */
	function onExportCsv(event) {
		var $tab = $(event.delegateTarget);

		var $button = $(event.target);
		if ($button.hasClass('disabled'))
			return;

		var pageParam = $.extend({},
			$tab.data('defaultParameters'),
			$tab.data('parameters'),
			$tab.data('constParameters'));

		var blsParam = SINGLEPAGE.BLS.getBlsParam(pageParam);

		blsParam.outputformat = 'csv';
		delete blsParam.number;
		delete blsParam.first;

		var url = new URI(BLS_URL).segment(pageParam.operation).addSearch(blsParam).toString();
		if (SINGLEPAGE.DEBUG) {
			console.log('CSV download url', url, blsParam);
		}

		$button
			.addClass('disabled')
			.attr('disabled', true)
			.prepend('<span class="fa fa-spinner fa-spin"></span>');
		$.ajax(url, {
			accepts: 'application/csv',
			cache: 'false',
			data: 'text',
			error: function(jqXHR, textStatus, errorThrown) {

			},
			success: function(data) {
				// NOTE: Excel <=2010 seems to ignore the BOM altogether, see https://stackoverflow.com/a/19516038
				var b = new Blob([data], { type: 'text/plain;charset=utf-8' });
				saveAs(b, 'data.csv'); // FileSaver.js
			},
			complete: function() {
				$button
					.removeClass('disabled')
					.attr('disabled', false)
					.find('.fa-spinner').remove();
			}
		});
	}

	/**
	 * 
	 */
	function onShortenLink(event) {
        
        var url = decodeURIComponent(window.location.toString());
        
        url = url.replace('localhost', 'eplab.artsci.wustl.edu');
        
        var url_bits = url.split('?');
        var query_string = url_bits.slice(1).join('?');
        
        var fixed_url = url_bits[0] + '?' + encodeURIComponent(encodeURI(query_string))
        
        $.get('https://earlyprint.wustl.edu/url/index.php?l=' + fixed_url, 
            function(short_url) {
                
                const el = document.createElement('textarea');
                el.value = short_url;
                document.body.appendChild(el);
                el.select();
                document.execCommand('copy');
                document.body.removeChild(el);

                alert('Shareable link ' + short_url + ' copied to clipboard.');
            }
        );
	}
    
	/**
	 * Redraws the table, pagination, hides spinners, shows/hides group indicator, shows the pagination/group controls, etc.
	 *
	 * @param {any} data the successful blacklab-server reply.
	 */
	function setTabResults(data) {
		var $tab = $(this);
		var html;
		var textDirection = SINGLEPAGE.INDEX.textDirection || 'ltr';
		// create the table
		if (data.hits && data.hits.length)
			html = formatHits(data, textDirection);
		else if (data.docs && data.docs.length)
			html = formatDocs(data, textDirection);
		else if ((data.hitGroups && data.hitGroups.length) || (data.docGroups && data.docGroups.length)) {
        
			html = formatGroups(data);
        }
		else {
			html = [
				'<thead>',
					'<tr><th><a>No results found</a></th></tr>',
				'</thead>',
				'<tbody>',
					'<tr>',
						'<td class="no-results-found">No results were found. Please check your query and try again.</td>',
					'</tr>',
				'</tbody>'
			];
		}

		function onTableContentsReplaced() {
			// first time opening the concordances for a group, load the first results
			$(this).find('.collapse').one('show.bs.collapse', function() {
				$(this).find('.loadconcordances').click();
			});
			$(this).find('.loadconcordances').on('click', loadConcordances);
			$(this).find('.viewconcordances').on('click', viewConcordances);
		}

		// Always do this, if an out-of-bounds request is made and no data is returned,
		// the pagination will still be accurate, allowing the user to go back to a valid page.
        
		//updatePagination($tab.find('.pagination'), data);
        
        /*
        $('.pagination').each(
            function() {
                updatePagination($(this), data)
            }
        );
        */
        
        $tab.find('.pagination').each(
            function() {
                updatePagination($(this), data)
            }
        );
        
		replaceTableContent($tab.find('.resultcontainer table'), html.join(''), onTableContentsReplaced);
		hideSearchIndicator($tab);
		$tab.find('.resultcontrols, .resultcontainer').show();
		$tab.data('results', data);

		// Hide/show the group view identifier and reset button
		// The values of these are set when the search is initiated in viewConcordances()
		// TODO when blacklab-server echoes the friendly name of the group, display that here and don't set any data
		// when initiating the search
		// TODO we set the value as fallback when no value currently set, as when first loading page, there was no initial search
		// where we know the group parameter.
		var $resultgroupdetails = $tab.find('.resultgroupdetails');
		if (data.summary.searchParam.viewgroup) {
			$resultgroupdetails.show();
			var $resultgroupname = $resultgroupdetails.find('.resultgroupname');
			if (!$resultgroupname.text())
				$resultgroupname.text(data.summary.searchParam.viewgroup);
		} else {
			$resultgroupdetails.hide();
		}

		var showWarning = !!(data.summary.stoppedRetrievingHits && !data.summary.stillCounting);
		$tab.find('.results-incomplete-warning').toggle(showWarning);
	}

	/**
	 * Clears displayed data, hides pagination, group indicator, group control, cached results, etc.
	 *
	 * @param {any} $tab
	 */
	function clearTabResults($tab) {
		$tab.find('.resultcontrols').hide();
		$tab.find('.resultcontainer').hide().find('table thead, table tbody').empty();
		$tab.find('.resultgroupdetails .resultgroupname').empty();
		$tab.removeData('results');
	}

	/**
	 * Set new search parameters for this tab. Does not mark tab for refresh or remove existing data.
	 *
	 * NOTE: pagination is never updated based on parameters, but instead drawn based on search response.
	 * @param {jquery} $tab - tab-pane containing all contents of tab
	 * @param {any} newParameters - object containing any updated parameter keys
	 * @param {boolean} [toPageState = false] whether to copy the parameter values to their ui elements
	 */
	function setTabParameters($tab, newParameters, toPageState) {

		// make a copy of the new parameters without groupBy and viewGroup if the parameters were meant for
		// a tab with a different operation
		if (newParameters.operation != null && newParameters.operation !== $tab.data('parameters').operation) {
			newParameters = $.extend({}, newParameters);
			// undefined so we don't overwrite our existing parameters
			newParameters.groupBy = undefined;
			newParameters.viewGroup = undefined;
		}

		// write new values while preserving original values
		var updatedParameters = $.extend($tab.data('parameters'), newParameters, $tab.data('constParameters'));
        
		// copy parameter values to their selectors etc
		if (toPageState) {
			$tab.find('select.groupselect').selectpicker('val', updatedParameters.groupBy);
			$tab.find('.casesensitive').prop('checked', updatedParameters.caseSensitive);
		}
	}

	/**
	 * Updates the internal parameters for a tab and executes a search if the tab is currently active.
	 *
	 * Any currently shown results are not cleared.
	 * Automatically unhides results containers and controls once search completes.
	 *
	 * @param {any} event where the data attribute holds all new parameters
	 */
	function onLocalParameterChange(event, parameters) {

		var $tab = $(this);

		setTabParameters($tab, parameters, true);
        
		$tab.removeData('results'); // Invalidate data to indicate a refresh is required

		if ($tab.hasClass('active')) // Emulate a reopen of the tab to refresh it
			$tab.trigger('tabOpen');
	}

	/**
	 * The core search trigger, named a little awkwardly because it autotriggers when a tab is made active/opens.
	 * We emulate the tab reopening to update the displayed search results when new search parameters are set/selected.
	 */
	function onTabOpen(/*event, data*/) {

		var $tab = $(this);
        
		var searchSettings = $.extend({}, $tab.data('defaultParameters'), $tab.data('parameters'), $tab.data('constParameters'));
                
        if (searchSettings.operation == 'hits') {
            searchSettings.sort = searchSettings.hitSort;
        }
        else {
            searchSettings.sort = searchSettings.docSort;
        }
        
        delete searchSettings.hitSort;
        delete searchSettings.docSort;
                
		// CORE does as little UI manipulation as possible, just shows a tab when required
		// so we're responsible for showing the entire results area.
		$('#results').show();
        
		var querySummary = SINGLEPAGE.BLS.getQuerySummary(searchSettings.pattern, searchSettings.within, searchSettings.filters);
		
        $('#searchFormDivHeader').show()
		.find('#querySummary').text(querySummary).attr('title', querySummary.substr(0, 1000));

		if ($tab.data('results')) {
			// Nothing to do, tab is already displaying data (this happens when you go back and forth between tabs without changing your query in between)
			// Still notify core so that when the url is copied out the current tab can be restored.
			SINGLEPAGE.CORE.onSearchUpdated(searchSettings);
			return;
		}

		// Not all configurations of search parameters will result in a valid search
		// Verify that we're not trying to view hits without a pattern to generate said hits
		// and warn the user if we are
		if (searchSettings.operation === 'hits' && (searchSettings.pattern == null || searchSettings.pattern.length === 0)) {
			replaceTableContent($tab.find('.resultcontainer table'),
				['<thead>',
					'<tr><th><a>No hits to display</a></th></tr>',
				'</thead>',
				'<tbody>',
					'<tr>',
						'<td class="no-results-found">No hits to display... (one or more of Lemma/PoS/Word is required).</td>',
					'</tr>',
				'</tbody>'
				].join('')
			);
			$tab.find('.resultcontainer').show();
			$tab.find('.resultcontrols').hide();
			$tab.data('results', {}); // Prevent refreshing search on next tab open
			return;
		}

		// All is well, search!
		showSearchIndicator($tab);
		SINGLEPAGE.CORE.onSearchUpdated(searchSettings);
		SINGLEPAGE.BLS.search(searchSettings,
			function onSuccess() {
				hideBlsError();
				$tab.data('fnSetResults').apply(undefined, Array.prototype.slice.call(arguments)); // call with original args
			},
			function onError() {
				hideSearchIndicator($tab);
				showBlsError.apply(undefined, Array.prototype.slice.call(arguments)); // call with original args
			}
		);
	}

	return {
		init: function() {
			// Hide the results area and deactivate all tabs to prevent accidental refreshes later.
			// Tabs are unhidden when a search is submitted.
			$('#results').hide();
			$('#resultTabs a').each(function() { $(this).tab('hide'); });
			$('.searchIndicator').hide();

			// See parameters type documentation in singlepage-bls.js
			$('#tabHits')
				.data('parameters', {})
				.data('defaultParameters', {
					page: 0,
					pageSize: 50,
					sampleMode: null,
					sampleSize: null,
					sampleSeed: null,
					wordsAroundHit: null,
					pattern: null,
					within: null,
					filters: null,
					sort: null,
					hitSort: null,
					docSort: null,
					groupBy: null,
					viewGroup: null,
					caseSensitive: null,
				})
				.data('constParameters', {
					operation: 'hits',
				})
				.data('fnSetResults', setTabResults.bind($('#tabHits')[0]));

			$('#tabDocs')
				.data('parameters', {})
				.data('defaultParameters', {
					page: 0,
					pageSize: 50,
					sampleMode: null,
					sampleSize: null,
					sampleSeed: null,
					wordsAroundHit: null,
					pattern: null,
					within: null,
					filters: null,
					sort: null,
					hitSort: null,
					docSort: null,
					groupBy: null,
					viewGroup: null,
					caseSensitive: null,
				})
				.data('constParameters', {
					operation: 'docs',
				})
				.data('fnSetResults', setTabResults.bind($('#tabDocs')[0]));

			$('#resultTabsContent .tab-pane').on('localParameterChange', onLocalParameterChange);
			$('#resultTabsContent .tab-pane').on('tabOpen', onTabOpen);
			$('#resultTabsContent .tab-pane').on('click', '.exportcsv', onExportCsv);
			$('#resultTabsContent .tab-pane').on('click', '.shortenlink', onShortenLink);

			// Forward show/open event to tab content pane
			$('#resultTabs a').on('show.bs.tab', function() {
				$($(this).attr('href')).trigger('tabOpen');
			});

			// use indirect event capture for easy access to the tab content-pane
			$('#resultTabsContent .tab-pane')
				.on('click', '[data-bls-sort]', function(event) {
                    
                    var pane = $(this).closest('.tab-pane');
                    var parent_id = $(pane).attr('id');
                    
					var sort = $(this).data('blsSort');
                    
                    var invert = '';
                    
                    if (parent_id == 'tabDocs') {
                        invert = ($(event.delegateTarget).data('parameters').docSort === sort);
                    }
                    
                    if (parent_id == 'tabHits') {
                        invert = ($(event.delegateTarget).data('parameters').hitSort === sort);
                    }
                    
                    if (parent_id == 'tabDocs') {
                        $(this).trigger('localParameterChange', {
                            docSort: invert ? '-' + sort : sort,
                            page: 0
                        });
                    }

                    if (parent_id == 'tabHits') {
                        $(this).trigger('localParameterChange', {
                            hitSort: invert ? '-' + sort : sort,
                            page: 0
                        });
                    }
                    
					event.preventDefault();
				})
				.on('click', '[data-page]', function(event) {
					$(this).trigger('localParameterChange', {
						page: $(this).data('page')
					});
					event.preventDefault();
				})
				.on('change', '.casesensitive', function(event) {
					$(this).trigger('localParameterChange', {
						caseSensitive: $(this).is(':checked') || null
					});
					event.preventDefault();
				})
				// don't attach to 'changed', as that fires every time a single option is toggled, instead wait for the menu to close
				.on('hide.bs.select', 'select.groupselect', function(event) {
					var prev = $(event.delegateTarget).data('parameters').groupBy || [];
					var cur = $(this).selectpicker('val') || [];
					// Don't fire search if options didn't change
					
					if (prev.length != cur.length || !prev.every(function(elem) { return cur.includes(elem); })) {
						$(this).trigger('localParameterChange', {
							groupBy: cur,
							page: 0,
							viewGroup: null, // Clear any group we may be currently viewing, as the available groups just changed
							sort:null,
						});
					}

					event.preventDefault();
				})
				.on('click', '.clearviewgroup', function(event) {
					$(this).trigger('localParameterChange', {
						page: 0,
						viewGroup: null,
						sort:null,
					});
					event.preventDefault();
				});
		},

		showCitation: showCitation,
		showProperties: showProperties,

		/**
		 * Set new search parameters and mark tabs for a refresh of data.
		 *
		 * The currently shown tab will auto-refresh.
		 * Parameters with corresponding UI-elements within the tabs will update those elements with the new data.
		 * NOTE: pagination is never updated based on parameters, but instead drawn based on search response.
		 * @param {any} searchParameters New search parameters.
		 * @param {boolean} [clearResults=false] Clear any currently displayed search results.
		 */
		setParameters: function(searchParameters, clearResults) {
			$('#resultTabsContent .tab-pane').each(function() {
				var $tab = $(this);
				if (clearResults)
					clearTabResults($tab);

				$tab.trigger('localParameterChange', searchParameters);
			});
		},

		/**
		 * Clear all results, hide the result area and reset all search parameters within the tabs.
		 *
		 * Deactivates all tabs and hides the result area.
		 */
		reset: function() {
			// Hide the results area and deactivate all tabs to prevent accidental refreshes later (search is executed when tab is opened (if search parameters are valid))
			$('#results').hide();
			$('#resultTabs a').each(function() { $(this).tab('hide'); });

			$('#searchFormDivHeader').hide();

			$('#resultTabsContent .tab-pane').each(function() {
				var $tab = $(this);

				clearTabResults($tab);
				$tab.trigger('localParameterChange', $.extend({},
					$tab.data('defaultParameters'),
					$tab.data('constParameters')
				));
			});
		}
	};
})();

function show_help(type_of_help) {

    var modified_type_of_help = type_of_help;
    if (modified_type_of_help == 'pos') {
        if (document.URL.indexOf('all') > -1) {
            modified_type_of_help = 'pos_all';
        }
        if (document.URL.indexOf('am') > -1) {
            modified_type_of_help = 'pos_am';
        }
        if (document.URL.indexOf('eebotcp') > -1) {
            modified_type_of_help = 'pos_eebotcp';
        }
    }

    $.get('/corpus-frontend-1.2/doc/help_' + modified_type_of_help + '.html?v=' + Math.floor(Math.random() * 1000), function(data, status) {
        $('#help_modal_text').html(data);
        $('#help_modal').css('display', 'block');
    });
}

function close_help() {
    $('#help_modal').css('display', 'none');
}

function empty_function() {
    event.stopPropagation();
}

function handle_title_toggle() {
    
    var properties_state = $($('.properties_column')[0]).css('display');

    if (properties_state == 'none') {
        $('.properties_column').css('display', 'table-cell');
        $('.metadata_column').css('display', 'none');
    }
    else {
        $('.properties_column').css('display', 'none');
        $('.metadata_column').css('display', 'table-cell');
    }
}

function neatly_trim_string(string_to_trim, desired_length) {

    if (string_to_trim == undefined) {
        return "";
    }

    var parts = string_to_trim[0].split(' ');

    var result = [];
    var running_length = 0;
    
    for (var a = 0; a < parts.length; a++) {
        if ((running_length + result.length + parts[a].length) > desired_length) {
            break;
        }
        result.push(parts[a]);
        running_length = running_length + parts[a].length;
    }

    return result.join(' ');
}

function handle_inline_kwic(docPid, docHits, docUrl) {
    
    var urlParams = new URLSearchParams(window.location.search);
    
    var patt = urlParams.get('patt');
    
    if ($('#doc_inline_tr_' + docPid).css('display') == 'none') {                
        
        var index_to_search = 'eebotcp';
        
        $.get('/blacklab-server-2.2.0/' + index_to_search + encodeURI('/hits?patt=' + patt + 
                '&docpid=' + docPid + 
                '&number=' + docHits),
        
            function(data){
                
                $('#doc_inline_td_' + docPid).html('');
                
                $(data).find('hit').each(
                    function() {
                
                        var left = [];
                        var match = [];
                        var right = [];
                        
                        $(this).find('left w').each(
                            function() {
                                left.push($(this).html());
                            }
                        );
                
                        $(this).find('match w').each(
                            function() {
                                match.push($(this).html());
                            }
                        );
                
                        $(this).find('right w').each(
                            function() {
                                right.push($(this).html());
                            }
                        );
                        
                        $('#doc_inline_td_' + docPid).append(
                            '<div>' + 
                            ' <span class="doc_inline_left">' + left.join(' ') + '</span>' +
                            ' <span class="doc_inline_match">' + match.join(' ') + '</span>' +
                            ' <span class="doc_inline_right">' + right.join(' ') + '</span>' +
                            '</div>'
                        );
                    }
                );
        
                $('#doc_inline_tr_' + docPid).css('display', 'table-row');
            }
        );
    }
    else {
        $('#doc_inline_tr_' + docPid).css('display', 'none');
    }
}

/*
function toggle_super_simple_more() {

    if ($('#simple_search_toggle_label').html() == 'more') {
        $('#simple_search_toggle_label').html('less');
        $('.simple_search_more').css('display', 'block');
    }
    else {
        $('#simple_search_toggle_label').html('more');
        $('.simple_search_more').css('display', 'none');
    }
    
}
*/

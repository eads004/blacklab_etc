#macro( filterfields $fields)
<div class="form-horizontal">
#foreach($docfield in $fields)
    <div class="form-group filterfield" id="$docfield.getId()" data-filterfield-type="$docfield.getType()"> ##- Behaves as a .row, so every child element should have some form of .col-*
        <label class="col-xs-12">$docfield.getDisplayName()</label>

        #if($docfield.getType() == "select" && !$docfield.getValidValues().isEmpty())
            <div class="col-xs-12">
                <select title="$docfield.getDisplayName()" class="selectpicker form-control" data-container="body" multiple>
            
                #foreach($pair in $docfield.getValidValues())
                    <option value="$pair.getValue()">$pair.getDescription()</option>
                #end
                </select>
            </div>
        #elseif($docfield.getType() == "range")
            <div class="col-xs-4">
                <input type="text" placeholder="From" class="form-control">
            </div>
            <div class="col-xs-4">
                <input type="text" placeholder="To" class="form-control">
            </div>
        #else
            <div class="col-xs-12">
                <input type="text" placeholder="$docfield.getDisplayName()" class="form-control"
                    #if($docfield.getType() == "combobox")
                        autocomplete="off" data-autocomplete="$docfield.getId()"
                    #end
                />
            </div>
        #end
    </div>
#end
</div>
#end

#macro( propfield $field)
    #set ($propertyFieldInputId = $propertyField.getId() + "_value")
    #set ($propertyFieldFileInputId = $propertyField.getId() + "_file")
    #set ($propertyFieldCaseId = $propertyField.getId() + "_case")

    <div class="form-group propertyfield" id="$propertyField.getId()"> ## behaves as .row when in .form-horizontal so .row may be omitted
        <label for="$propertyFieldInputId" class="col-xs-12 col-md-3">$propertyField.getDisplayName()</label>
        <div class="col-xs-12 col-md-9 upload-button-container">
            
            #if($field.getType() == "select" && !$field.getValidValues().isEmpty())
                <select id="$propertyFieldInputId" title="$field.getDisplayName()" class="selectpicker form-control" data-container="body">
                    <option value="">choose value</option>
                #foreach($pair in $field.getValidValues())
                    <option value="$pair.getValue()">$pair.getDescription()</option>
                #end
                </select>
            #else
                <input type="text" id="$propertyFieldInputId" name="$propertyFieldInputId" placeholder="$propertyField.getDisplayName()" class="form-control"
                    #if($field.getType() == "combobox") 
                        ## when autocompleting word properties, blacklab needs to know the complex field from which they originate
                        ## the format is complexfield/property?term=searchterm
                        data-autocomplete="$propertyField.getComplexFieldName()/$propertyField.getId()" 
			autocomplete="off"
		    #end
                >
                ## word-list upload-button only for text inputs
                <span class="btn btn-default upload-button">
                    <input type="file" id="$propertyFieldFileInputId" title="Upload a list of values">
                    <span class="glyphicon glyphicon-open"></span> 
                </span>
            #end

            #if($propertyField.isCaseSensitive())
            <div class="checkbox"><label for="$propertyFieldCaseId"><input type="checkbox" id="$propertyFieldCaseId" name="$propertyFieldCaseId"> Case&nbsp;and&nbsp;diacritics&nbsp;sensitive</label></div>
            #end
        </div>
    </div>
#end

#parse("header.vm")

#if($propertyGroups.isEmpty()) ## Only display ungrouped fields if no other groups are present
	#set($_ = $propertyGroups.put("Properties", $ungroupedPropertyFields)) ## silence return value of put() to stop it from ending up in the page.
#end
#if($metadataGroups.isEmpty()) ## Only display ungrouped fields if no other groups are present
    #set($_ = $metadataGroups.put("Metadata", $ungroupedMetadataFields)) ## silence return value of put() to stop it from ending up in the page.
#end
        

#set($unescapedIndexStructureJson = $indexStructureJson) ## variables with 'unescaped' in the name are not html-escaped, see BaseResponse.java

<script>
    var SINGLEPAGE = SINGLEPAGE || {};
    SINGLEPAGE.INDEX = JSON.parse("$esc.javascript($unescapedIndexStructureJson)");
    var BLS_URL = "$blsUrl";
    var PROPS_IN_COLUMNS = "$websiteConfig.getPropColumns()".split(',');
</script>

<link rel="stylesheet" href="$pathToTop/css/cql_querybuilder.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.19.1/URI.min.js" integrity="sha256-D3tK9Rf/fVqBf6YDM8Q9NCNf/6+F2NOKnYSXHcl0keU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js" integrity="sha256-FPJJt8nA+xL4RU6/gsriA8p8xAeLGatoyTjldvQKGdE=" crossorigin="anonymous"></script>
<script src="$pathToTop/js/vendor/lucene-query-parser.js"></script> ## https://github.com/thoward/lucene-query-parser.js

<script src="$pathToTop/js/singlepage.js?_$cache"></script>
<script src="$pathToTop/js/singlepage-bls.js?_$cache"></script>
<script src="$pathToTop/js/singlepage-form.js?_$cache"></script>
<script src="$pathToTop/js/singlepage-interface.js?_$cache"></script>
<script src="$pathToTop/js/cql_querybuilder.js?_$cache"></script>
<script src="$pathToTop/js/singlepage-cqlparser.js?_$cache"></script>

<!--
<noscript>
    <br/><br/><br/>
    <div class="alert alert-error">
        <h1>Error: JavaScript disabled.</h1>
        We notice that you have JavaScript disabled in your browser. This website requires JavaScript to be enabled. Please enable JavaScript to use this website.
    </div>
</noscript>
-->


<form onsubmit="return SINGLEPAGE.CORE.searchSubmit();" class="clearfix">

<div id='searchFormDiv' class="col-xs-12 contentbox collapse in">
    #if($websiteConfig.getCorpusOwner())<a href="$pathToTop" class="navbar-logo-margin"><i class="fa fa-arrow-left"></i> back to my corpora</a>#end
    <h2>$websiteConfig.getCorpusDisplayName() #if($websiteConfig.getCorpusOwner())<span class="small">($websiteConfig.getCorpusOwner())</span>#end</h2>

    <div class="row">
        <div class="col-xs-12 col-md-6" id="searchContainer">
            <h3>Search with &hellip;</h3>
            <ul class="nav nav-tabs" id="searchTabs">
                <li class="active"><a href="#simple" data-toggle="tab" class="querytype">Word or phrase</a></li>
                <li><a href="#advanced" data-toggle="tab" class="querytype">Query Builder</a></li>
                <li><a href="#query" data-toggle="tab" class="querytype">CQL</a></li>
            </ul>
            <div class="tab-content">
		        #set($usePropertyTabs = ($propertyGroups.size() > 1)) ##only use tabs when we have more than one group to display
                <div class="tab-pane active form-horizontal" id="simple" #if ($usePropertyTabs) style="margin-top: -15px"#end>
			        #if($usePropertyTabs) 
			            <ul class="nav nav-tabs subtabs">
			            #foreach($group in $propertyGroups.entrySet())
			                #set($groupName = $group.key)
			                #set($groupId = $groupName.replace(" ", "").concat("_prop"))
			                <li #if($velocityCount == 1) class="active" #end>
			                    <a href="#$groupId" data-toggle="tab">$groupName</a>
			                </li>
			                #set($firstGroup = "false")
			            #end
			            </ul>
			            <div class="tab-content">
			            #foreach($group in $propertyGroups.entrySet())
			                #set($groupName = $group.key)
			                #set($groupId = $groupName.replace(" ", "").concat("_prop"))
			                <div class="tab-pane #if($velocityCount == 1) active #end" id="$groupId">
			                    #foreach($propertyField in $group.value)
			                        #propfield($propertyField)
			                    #end
			                </div>
			                #set($firstGroup = "false")            
			            #end
			            </div>
			        #else ## !$usePropertyTabs, insert fields directly

<!-- REPLACE LOGIC DRIVEN BY $propertyGroups.entrySet() WITH A FIXED SET OF VALUES -->

 <div class="tab-pane active form-horizontal" id="simple">

    <div class="form-group propertyfield" id="reg">
      <label for="reg_value" class="col-xs-12 col-md-4">Regularized spelling(s)</label>
      <div class="col-xs-12 col-md-1 help_widget"><a href="javascript:show_help('reg')">?</a></div>
      <div class="col-xs-12 col-md-7 upload-button-container">
        <input type="text" id="reg_value" name="reg_value" placeholder="reg" class="form-control"> <span class=
        "btn btn-default upload-button"><input type="file" id="reg_file" title="Upload a list of values"></span>
        <div class="checkbox">
          <label for="reg_case"><input type="checkbox" id="reg_case" name="reg_case">
          Case&nbsp;and&nbsp;diacritics&nbsp;sensitive</label>
        </div>
      </div>
    </div>

    <div class="form-group propertyfield" id="word">
      <label for="word_value" class="col-xs-12 col-md-4">Original spelling(s)</label>
      <div class="col-xs-12 col-md-1 help_widget"><a href="javascript:show_help('word')">?</a></div>
      <div class="col-xs-12 col-md-7 upload-button-container">
        <input type="text" id="word_value" name="word_value" placeholder="word" class="form-control"> <span class=
        "btn btn-default upload-button"><input type="file" id="word_file" title="Upload a list of values"></span>
        <div class="checkbox">
          <label for="word_case"><input type="checkbox" id="word_case" name="word_case">
          Case&nbsp;and&nbsp;diacritics&nbsp;sensitive</label>
        </div>
      </div>
    </div>

    <div class="form-group propertyfield" id="lemma">
      <label for="lemma_value" class="col-xs-12 col-md-4">Lemma(s)</label>
      <div class="col-xs-12 col-md-1 help_widget"><a href="javascript:show_help('lem')">?</a></div>
      <div class="col-xs-12 col-md-7 upload-button-container">
        <input type="text" id="lemma_value" name="lemma_value" placeholder="lemma" class="form-control"> <span class=
        "btn btn-default upload-button"><input type="file" id="lemma_file" title="Upload a list of values"></span>
        <div class="checkbox">
          <label for="lemma_case"><input type="checkbox" id="lemma_case" name="lemma_case">
          Case&nbsp;and&nbsp;diacritics&nbsp;sensitive</label>
        </div>
      </div>
    </div>

    <div class="form-group propertyfield" id="pos">
      <label for="pos_value" class="col-xs-12 col-md-4">Part(s) of speech</label>
      <div class="col-xs-12 col-md-1 help_widget"><a href="javascript:show_help('pos')">?</a></div>
      <div class="col-xs-12 col-md-7 upload-button-container">
        <input type="text" id="pos_value" name="pos_value" placeholder="pos" class="form-control"> <span class=
        "btn btn-default upload-button"><input type="file" id="pos_file" title="Upload a list of values"> </span>
      </div>
    </div>

    <div class="form-group propertyfield" id="id">
      <label for="id_value" class="col-xs-12 col-md-4">id</label>
      <div class="col-xs-12 col-md-8 upload-button-container">
        <input type="text" id="id_value" name="id_value" placeholder="id" class="form-control"> <span class=
        "btn btn-default upload-button"><input type="file" id="id_file" title="Upload a list of values"> </span>
      </div>
    </div>

    <div class="form-group" id="div_simplesearch_within">
      <label class="col-xs-12 col-md-4">Within:</label>
      <div class="btn-group col-xs-12 col-md-8" data-toggle="buttons" id="simplesearch_within" style="display:block;">
        <label class="btn btn-default active"><input type="radio" autocomplete="off" name="within" value="" checked=
        "checked">document</label> <label class="btn btn-default"><input type="radio" autocomplete="off" name="within"
        value="p">paragraph</label> <label class="btn btn-default"><input type="radio" autocomplete="off" name="within"
        value="s">sentence</label>
      </div>
    </div>

  </div>


<!-- END REPLACEMENT -->

			        #end
                    <div class="form-group" id="div_simplesearch_within">
                        ## TODO extract available options from blacklab
                        <label class="col-xs-12 col-md-3">Within:</label>
                        
                        <div class="btn-group col-xs-12 col-md-9" data-toggle="buttons" id="simplesearch_within" style="display:block;">
                            <label class="btn btn-default active">
                                <input type="radio" autocomplete="off" name="within" value="" checked="checked">document
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" autocomplete="off" name="within" value="p">paragraph
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" autocomplete="off" name="within" value="s">sentence
                            </label>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="advanced">
                    <div id="querybuilder"></div>
                </div>
                <div class="tab-pane" id="query">
                    <h3>Corpus Query Language:</h3>
                    <textarea id="querybox" class="form-control" name="querybox" rows="7"></textarea>
                    <button type="button" class="btn btn-sm btn-default" name="parseQuery" id="parseQuery" title="Edit your query in the querybuilder">Copy to query builder</button>                
                    <span id="parseQueryError" class="text-danger" style="display:none;"><span class="fa fa-danger"></span> The querybuilder could not parse your query</span>
                </div>
            </div>
        </div>

        #set($useFilterTabs = ($metadataGroups.size() > 1)) ##only use tabs when we have more than one group to display
        <div class="col-xs-12 col-md-6" id="filterContainer">
            <h3>Filter search by &hellip;</h3>
            
        #if($useFilterTabs) 
            <ul class="nav nav-tabs">
            #foreach($group in $metadataGroups.entrySet())
                #set($groupName = $group.key)
                #set($groupId = $groupName.replace(" ", "").concat("_meta"))
                <li #if($velocityCount == 1) class="active" #end>
                    <a href="#$groupId" data-toggle="tab">$groupName</a>
                </li>
                #set($firstGroup = "false")
            #end
            </ul>
            <div class="tab-content">
            #foreach($group in $metadataGroups.entrySet())
                #set($groupName = $group.key)
                #set($groupId = $groupName.replace(" ", "").concat("_meta"))
                <div class="tab-pane #if($velocityCount == 1) active #end" id="$groupId">
                    #filterfields($group.value)
                </div>
                #set($firstGroup = "false")            
            #end
            </div>
        #else ## $useFilterTabs

<!-- REPLACING AUTO-GENERATED CODE -->

                <div class="tab-content">

    <br/>
    <br/>

    <div class="form-group filterfield" id="author" data-filterfield-type="combobox">
      <label class="col-xs-12 col-md-4">author</label>
      <div class="col-xs-12 col-md-1 help_widget"><a href="javascript:show_help('author')">?</a></div>
      <div class="col-xs-12 col-md-7">
        <input type="text" placeholder="author" class="form-control ui-autocomplete-input" autocomplete="off" data-autocomplete="author">
      </div>
    </div>
    <br/>
    <br/>

    <div class="form-group filterfield" id="title" data-filterfield-type="combobox">
      <label class="col-xs-12 col-md-4">title</label>
      <div class="col-xs-12 col-md-1 help_widget"><a href="javascript:show_help('title')">?</a></div>
      <div class="col-xs-12 col-md-7">
        <input type="text" placeholder="title" class="form-control ui-autocomplete-input" autocomplete="off" data-autocomplete="title">
      </div>
    </div>
    <br/>
    <br/>

    <div class="form-group filterfield" id="year" data-filterfield-type="combobox">
      <label class="col-xs-12 col-md-4">year</label>
      <div class="col-xs-12 col-md-1 help_widget"><a href="javascript:show_help('year')">?</a></div>
      <div class="col-xs-12 col-md-7">
        <input type="text" placeholder="year" class="form-control ui-autocomplete-input" autocomplete="off" data-autocomplete="year">
      </div>
    </div>
    <br/>
    <br/>

                </div>

<!-- END REPLACEMENT -->

        #end
            <div class="row">
                <div class="col-xs-8"><span id="filteroverview"></span></div>
            </div>
        </div>

        <div class="col-xs-12">
            <hr>
            <div style="display:inline-block; position: absolute; bottom: 0;">
                <input type="submit" class="btn btn-primary btn-lg" value="Search">
                <a href="?" onclick="return SINGLEPAGE.CORE.resetPage();" class="btn btn-default btn-lg" title="Start a new search">Reset</a>
            </div>
            
            <button type="button" class="btn btn-lg btn-default pull-right" data-toggle="modal" data-target="#settingsModal"><span class="glyphicon glyphicon-cog" style="vertical-align:text-top;"></span></button>
        </div>
    </div>
</div>

<div id="searchFormDivHeader" style="display:none;" class="contentbox col-xs-12">
    Results for: <span id="querySummary" class="small text-muted"></span> 
</div>

<div id="settingsModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Global settings</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">

                    <div class="form-group"> <!-- behaves as .row when in .form-horizontal so .row may be omitted -->
                        <label for="resultsPerPage" class="col-xs-3">Results per page:</label>
                        <div class="col-xs-9">
                            <select id="resultsPerPage" name="resultsPerPage" class="selectpicker" data-width="auto" data-style="btn-default">
                                <option value="20">20 results</option>
                                <option value="50">50 results</option>
                                <option value="100">100 results</option>
                                <option value="200">200 results</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sampleSize" class="col-xs-3">Sample size:</label>
                        <div class="col-xs-9">
                            <div class="input-group">
                                <select  id="sampleMode" name="sampleMode" class="selectpicker" data-width="auto" data-style="btn-default">
                                    <option value="percentage" selected>percentage</option>
                                    <option value="count">count</option>
                                </select>

                                <input id="sampleSize" name="sampleSize" placeholder="sample size" type="number" class="form-control"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sampleSeed" class="col-xs-3">Seed:</label>
                        <div class="col-xs-9">
                            <input id="sampleSeed" name="sampleSeed" placeholder="seed" type="number" class="form-control" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="wordsAroundHit" class="col-xs-3">Context size:</label>
                        <div class="col-xs-9">
                            <input id="wordsAroundHit" name="wordsAroundHit" placeholder="context Size" type="number" class="form-control" >
                        </div>
                    </div>
                    
                    <hr>
                   
                    <div class="checkbox-inline"><label for="wide-view"><input type="checkbox" id="wide-view" name="wide-view" data-persistent 
                    onChange="$('.container, .container-fluid').toggleClass('container', !$(this).is(':checked')).toggleClass('container-fluid', $(this).is(':checked'));"> Wide View</label></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" name="closeSettings" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="errorDiv" class="col-xs-12 alert alert-danger" style="display:none;">
    <h1>Error</h1>
    <p id="errorMessage"></p>
    <p>(for assistance, please contact <a href='mailto:servicedesk@ivdnt.org'>servicedesk@ivdnt.org</a>)</p>
</div>

<div class="col-xs-12 contentbox" id="results">
    <div id='totalsReport'>
        <div id="totalsReportText" class="pull-right"></div>
        <span id='totalsSpinner' class="fa fa-spinner fa-spin searchIndicator" style="font-size:16px; padding: 4px; margin: 0px 10px;"></span>
        <div id="totalsLimitedWarning" class="text-danger text-center" style="margin: 0px 10px;">
            <span class="fa fa-exclamation-triangle text-danger" style="font-size: 20px;"></span>
            <br>
            Too many results!
        </div>
    </div>

    <!-- No initially active tab by design -->
    <ul id="resultTabs" class="nav nav-tabs">
        <li><a href="#tabHits" data-toggle="tab">Per Hit</a></li>
        <li><a href="#tabDocs" data-toggle="tab">Per Document</a></li>
    </ul>

    <div id="resultTabsContent" class="tab-content">
        <div id="tabHits" class="tab-pane">
               ## control header
            <div class="resultcontrols">
                
                ## main controls
                <div style="display: flex;justify-content: space-between;align-items: flex-start;">
                    <div style="display: flex; flex-wrap: wrap; min-width: 220px; max-width: 100%;"> ## grouping + warnings  
                        
                        <div class="groupselect-container" style="display:flex;align-items:center;flex-wrap:nowrap;">
                            <select class="selectpicker groupselect" title="Group hits by..." data-size="15" data-actions-box="true" data-deselect-all-text="reset" data-show-subtext="true" data-style="btn-default btn-sm" multiple>
                                <optgroup label="Before hit">
                                   	#foreach($group in $propertyGroups.entrySet()) #foreach($propertyField in $group.value)
                                    <option id="option_aaaa_$propertyField.getId()" value="wordleft:$propertyField.getId()" data-content="Group by $propertyField.getDisplayName() <small class=&quot;text-muted&quot;>before</small>">Group by $propertyField.getDisplayName()</option>
                                    #end #end
                                </optgroup>
                                <optgroup label="Hit">
                                    #foreach($group in $propertyGroups.entrySet()) #foreach($propertyField in $group.value)
                                    <option id="option_bbbb_$propertyField.getId()" value="hit:$propertyField.getId()">Group by $propertyField.getDisplayName()</option>
                                    #end #end
                                </optgroup>
                                <optgroup label="After hit">
                                    #foreach($group in $propertyGroups.entrySet()) #foreach($propertyField in $group.value)
                                    <option id="option_cccc_$propertyField.getId()" value="wordright:$propertyField.getId()" data-content="Group by $propertyField.getDisplayName() <small class=&quot;text-muted&quot;>after</small>">Group by $propertyField.getDisplayName()</option>
                                    #end #end
                                </optgroup>
                                #foreach($group in $metadataGroups.entrySet())
                                <optgroup label="$group.key">
                                    #foreach($metadataField in $group.value)
                                    <option id="option_dddd_$metadataField.getId()" value="field:$metadataField.getId()">Group by  $metadataField.getDisplayName()</option>
                                    #end
                                </optgroup>
                                #end
                            </select>
                            <button type="button" class="btn btn-sm btn-default dummybutton">update</button> ## dummy button... https://github.com/INL/corpus-frontend/issues/88
                            <div class="checkbox-inline" style="margin-left: 5px;">
                                <label title="Separate groups for differently cased values" style="white-space: nowrap; margin: 0;" for="casesensitive-groups-hits"><input type="checkbox" class="casesensitive" id="casesensitive-groups-hits" name="casesensitive-groups-hits">Case sensitive</label>
                            </div>
                        </div>
                               
                        <div class="resultgroupdetails btn btn-sm btn-default nohover" style="margin-right: 5px">
                            <span class="fa fa-exclamation-triangle text-danger"></span> Viewing group <span class="resultgroupname"></span> &mdash; <a class="clearviewgroup" href="javascript:void(0)">Go back</a>
                        </div>
                        
                        <div class="results-incomplete-warning btn btn-sm btn-default nohover">
                            <span class="fa fa-exclamation-triangle text-danger"></span> Too many results! &mdash; your query was limited
                        </div>
                    </div>
                    
                    <div style="flex: 0 1000 auto"> ## buttons
                        <button type="button" class="btn btn-default btn-sm pull-right exportcsv" style="margin-left: 5px;margin-bottom: 5px;">Export CSV</button>
                        <button type="button" class="btn btn-danger btn-sm pull-right" data-toggle="collapse" data-target=".doctitle" style="margin-left: 5px;margin-bottom: 5px;">Show/hide titles</button>
                    </div>
                                        
                </div> 

                ## pagination
                <ul class="pagination pagination-sm"></ul>
            </div>
     
            <span class="fa fa-spinner fa-spin searchIndicator" style="position:absolute; left: 50%; top:15px"></span>

            <div class="lightbg haspadding resultcontainer">
                <table class="resultstable">
                <tbody>
                </tbody>
                </table>
            </div>
        </div> <!-- tabHits -->

        <div id="tabDocs" class="tab-pane">
            
            ## control header
            <div class="resultcontrols">
                
                ## main controls
                <div style="display: flex;justify-content: space-between;align-items: flex-start;">
                    <div style="display: flex; flex-wrap: wrap; min-width: 220px; max-width: 100%;"> ## grouping + warnings  
                        <div class="groupselect-container" style="display:flex;align-items:center;flex-wrap:nowrap;">
                            <select class="selectpicker groupselect" title="Group documents by..." data-size="15" data-actions-box="true" data-deselect-all-text="reset" data-style="btn-default btn-sm" multiple>
                                #foreach($group in $metadataGroups.entrySet())
                                <optgroup label="$group.key">
                                    #foreach($metadataField in $group.value)
                                    <option value="field:$metadataField.getId()">Group by EEEE $metadataField.getDisplayName()</option>
                                    #end
                                </optgroup>
                                #end
                            </select>
                            <button type="button" class="btn btn-sm btn-default dummybutton">update</button> ## dummy button... https://github.com/INL/corpus-frontend/issues/88
                            <div class="checkbox-inline" style="margin-left: 5px;">
                                <label title="Separate groups for differently cased values" style="white-space: nowrap; margin: 0;" for="casesensitive-groups-docs"><input type="checkbox" class="casesensitive" id="casesensitive-groups-docs" name="casesensitive-groups-docs">Case sensitive</label>
                            </div>
                        </div>
                    
                        <div class="resultgroupdetails btn btn-sm btn-default nohover" style="margin-right: 5px;>
                            <span class="fa fa-exclamation-triangle text-danger"></span> Viewing group <span class="resultgroupname"></span> &mdash; <a class="clearviewgroup" href="javascript:void(0)">Go back</a>
                        </div>
                        <div class="results-incomplete-warning btn btn-sm btn-default nohover">
                            <span class="fa fa-exclamation-triangle text-danger"></span> Too many results! &mdash; your query was limited
                        </div>
                    </div>
                    <div style="flex: 0 1000 auto"> ## buttons
                        <button type="button" class="btn btn-default btn-sm pull-right exportcsv" style="margin-left: 5px;margin-bottom: 5px;">Export CSV</button>
                    </div>
                    
                </div> 

                ## pagination
                <ul class="pagination pagination-sm"></ul>
            </div>

            <span class="fa fa-spinner fa-spin searchIndicator" style="position:absolute; left: 50%; top:15px"></span>

            <div class="lightbg haspadding resultcontainer">
                <table class="resultstable">
                <tbody>
                </tbody>
                </table>
            </div>
        </div> <!-- tabDocs -->
    </div>

    </div>

</form>

<div id="help_modal">
    <div id="help_modal_content">
        <div id="help_modal_close"><a href="javascript:close_help();">close</a></div>
        <div id="help_modal_clear"> </div>
        <div id="help_modal_text"></div>
    </div>
</div>

#parse("footer.vm")


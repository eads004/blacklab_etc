# Earlyprint's blacklab code



    http://localhost:8080/corpus-frontend-1.2/eebotcp/search/

    http://eplab.artsci.wustl.edu/blacklab-server-2.2.0/eebotcp/hits?patt=%22test%22

    https://eplab.artsci.wustl.edu/corpus-frontend-1.2/eebotcp/search/


## PROBLEMS w/ frontend 1.2 and blacklab 2.2.0##

**All fixed?**

    1.  Docs tab doesn't work.  

        http://localhost:8080/blacklab-server-2.2.0/eebotcp/docs

        http://localhost:8080/blacklab-server-2.2.0/eebotcp/docs?patt=%22tree%22&sort=numhits

    2.  Group by dropdown not populated.

    3.  Toggle metadata button doesn't work.

    4.  CSV button doesn't work

    5.  Does shareable link button work?

    6.  URL pasted in: does it work?



## BlackLab-2.2.0

"Forked" from (https://github.com/INL/BlackLab)[https://github.com/INL/BlackLab] .  Not yet modified.

Running with the latest java.  See https://petewan.com/blog-post/install-and-manage-multiple-java-jdk-and-jre-versions-on-ubuntu-20-04/ for java on Ubuntu.

    sudo apt install openjdk-17-jdk

    java -version

    openjdk version "17.0.2" 2022-01-18
    OpenJDK Runtime Environment (build 17.0.2+8-Ubuntu-118.04)
    OpenJDK 64-Bit Server VM (build 17.0.2+8-Ubuntu-118.04, mixed mode, sharing)

    javac -version

    javac 17.0.2

Builds fine:

    cd BlackLab-2.2.0

    export PATH=/home/spenteco/Downloads/apache-maven-3.6.3/bin:$PATH
    
    export PATH=/Users/pentecost/0/apache-maven-3.8.6/bin:$PATH
    

    mvn package
    
    sudo cp server/target/blacklab-server-2.2.0.war /var/lib/tomcat8/webapps/
    
    sudo cp server/target/blacklab-server-2.2.0.war /Library/Tomcat/webapps/
    
    
https://wolfpaulus.com/tomcat/
    

ON AWS:  /opt/tomcat/latest/webapps


Indexing: see rebuild_ep/blacklab.2.2.0.  Sees to work just like 1.7.3.

Url's:

    http://localhost:8080/blacklab-server-2.2.0/eebotcp/hits?patt=%22test%22

    http://localhost:8080/corpus-frontend-1.2/eebotcp/search/

## BlackLab-1.7.3

"Forked" from (https://github.com/INL/BlackLab)[https://github.com/INL/BlackLab] and lightly modified.  


### To build

(Note that build requires Maven . . . )

    git clone https://spentecost@bitbucket.org/eads004/blacklab_etc.git

    cd blacklab_etc/BlackLab-1.7.3

    export PATH=/home/spenteco/Downloads/apache-maven-3.6.3/bin:$PATH

    mvn package

    sudo cp server/target/blacklab-server-1.7.3.war /var/lib/tomcat8/webapps/

Or

    scp server/target/blacklab-server-1.7.3.war spenteco@ada.artsci.wustl.edu:~/

then move the war file around on ada.

Note: tomcat folder on eplab is 

    /opt/tomcat/latest

## corpus-frontend-2.1.0

corpus-frontend-1.2 works with Blacklab 2, so we're still using it.


## corpus-frontend-1.2

"Forked" from (https://github.com/INL/corpus-frontend)[https://github.com/INL/corpus-frontend]) and customized.  We're at version 1.2, because when we started the project (and started customizing the front-end), the latest version wasn't quite done.

### To build

(Note that build requires Maven . . . )

    git clone https://spentecost@bitbucket.org/eads004/blacklab_etc.git

    cd blacklab_etc/corpus-frontend-1.2

    export PATH=/home/spenteco/Downloads/apache-maven-3.6.3/bin:$PATH

    mvn package
    
    sudo cp target/corpus-frontend-1.2.war /var/lib/tomcat8/webapps/
    
    sudo cp target/corpus-frontend-1.2.war /Library/Tomcat/webapps/

Or

    scp target/corpus-frontend-1.2.war spenteco@ada.artsci.wustl.edu:~/

Then move the war file around on the ada.


## corpus-frontend-2.1.0

    export PATH=/home/spenteco/Downloads/apache-maven-3.6.3/bin:$PATH

    mvn package
    
    sudo cp target/corpus-frontend-2.1.0.war /var/lib/tomcat8/webapps/



    ON EPLAB /opt/tomcat/apache-tomcat-9.0.54/webapps/


## config

2 pair of files, one for ada, and one for my local installation.  On ada, they install at
 /etc/blacklab/
 
    # Proxy for blacklab
    <LocationMatch "/proxy_blacklab/">
         ProxyPass http://localhost:8080/blacklab-server-2.2.0/
         Header set Access-Control-Allow-Origin: *
    </LocationMatch>

    <LocationMatch "/blacklab-server-1.7.3">
         ProxyPass http://localhost:8080/blacklab-server-2.2.0/
         Header set Access-Control-Allow-Origin: *
    </LocationMatch>

    <LocationMatch "/blacklab-server-2.2.0">
         ProxyPass http://localhost:8080/blacklab-server-2.2.0/
         Header set Access-Control-Allow-Origin: *
    </LocationMatch>

    <LocationMatch "/corpus-frontend-2.1.0">
         ProxyPass http://localhost:8080/corpus-frontend-2.1.0
         Header set Access-Control-Allow-Origin: *
    </LocationMatch>
 


## Handy URL's . . .   

    http://eplab.artsci.wustl.edu/blacklab-server-2.2.0/eebotcp/hits?patt="tree"

    http://eplab.artsci.wustl.edu/corpus-frontend-2.1.0/eebotcp/search/




    http://eplab.artsci.wustl.edu/blacklab-server-1.7.3/eebotcp/

 . . . which were in my notes, and which I don't want to lose.  These, especially,

    http://localhost:8080/corpus-frontend-1.2/eebotcp/search/
    
    http://localhost:8080/corpus-frontend-1.2/eebotcp/search/

    http://localhost:8080/blacklab-server-1.7.3/eebotcp/hits?patt="test"

    http://ada.artsci.wustl.edu:8080/corpus-frontend-1.2/eebotcp/search/

    http://ada.artsci.wustl.edu:8080/blacklab-server-1.7.3/eebotcp/hits?patt="test"
 
And these, sort of,

    http://ada.artsci.wustl.edu:8080/blacklab-server-1.7.3/eebotcp/?listvalues=pos

    http://ada.artsci.wustl.edu:8080/blacklab-server-1.7.3/eebotcp/hits?patt="tree"

    http://localhost:8080/blacklab-server-1.7.3/all/?listvalues=author

    http://localhost:8080/blacklab-server-1.7.3/all/?listvalues=pos

    http://localhost:8080/blacklab-server-1.7.3/am_ix/?listvalues=pos

    http://localhost:8080/blacklab-server-1.7.3/all/hits?patt="tree"

    http://localhost:8080/blacklab-server-1.7.3/am_ix/hits?patt=[pos=%22christian%22]

    http://localhost:8080/blacklab-server-1.7.3/am_ix/?listvalues=pos

    http://localhost:8080/blacklab-server-1.7.3/all/hits?patt=[reg="tree"]

    http://localhost:8080/blacklab-server-1.7.3/all/hits?patt="tree"

    http://localhost:8080/blacklab-server-1.7.3/all/docs?patt=%22tree%22&sort=field:year

    http://localhost:8080/blacklab-server-1.7.3/all/docs?patt=%22tree%22&sort=numhits

    http://localhost:8080/blacklab-server-1.7.3/all/docs?patt=%22tree%22&facets=field:year

    http://localhost:8080/blacklab-server-1.7.3/all/docs?patt=%22tree%22&group=field:year

    http://localhost:8080/blacklab-server-1.7.3/all/hits?patt=%22tree%22&group=field:year

    http://localhost:8080/blacklab-server-1.7.3/all/docs?patt=[pos="p-acp"][pos="ro-crq"]

    http://localhost:8080/blacklab-server-1.7.3/all/docs?patt=[pos="n1"][pos="vvz"]

    http://localhost:8080/blacklab-server-1.7.3/all/docs?patt=[pos="dt"][pos="n1"][pos="vvz"]

    http://localhost:8080/blacklab-server-1.7.3/all/docs?patt=[pos="p-acp"][pos="ro-crq"][pos="dt"]

    http://localhost:8080/blacklab-server-1.7.3/all/docs?patt=[pos="ro-crq"][pos="dt"][pos="n1"][pos="vvz"]

    http://localhost:8080/blacklab-server-1.7.3/all/docs?patt=[pos="p-acp"][pos="ro-crq"][pos="dt"][pos="n1"][pos="vvz"]
    http://localhost:8080/corpus-frontend-1.2/am_ix/search/hits?number=20&first=0&patt=%5Breg%3D%22the%22%5D%5B%5D%7B1%2C1%7D%5Breg%3D%22tree%22%5D&sort=field%3Asearch_year

    location.href='https://tinyurl.com/create.php?url='+encodeURIComponent('http://localhost:8080/corpus-frontend-1.2/am_ix/search/hits?number=20&first=0&patt=%5Breg%3D%22the%22%5D%5B%5D%7B1%2C1%7D%5Breg%3D%22tree%22%5D&sort=field%3Asearch_year');


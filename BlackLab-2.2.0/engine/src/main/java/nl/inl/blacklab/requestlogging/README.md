Base class for logging requests and cache statistics to e.g. a SQLite 
database.

Should be removed in favour of the instrumentation module and e.g.
Prometheus.
